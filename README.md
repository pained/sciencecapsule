# Science Capsule

## Using Science Capsule bare-metal (without containers)

### Installation

- These steps were successfully tested on:
    - Linux (Ubuntu 18.04.4 and 20.04)
    - macOS (10.12.6)
    - Windows (10.0.10240)

#### Prerequisites

The following tools should be available on the system to be able to install and run Science Capsule:

- `conda` package manager
- Git
- (Linux only, optional) `inotifywait` (generally distributed in a package together with related utilties, e.g. `inotifytools`)
- (Linux only, optional) `strace`
    - Enabling `strace` for Science Capsule might require additional steps. Refer to [this document](./docs/enabling-strace.md) for a detailed description.

#### Science Capsule repository

Clone the `sciencecapsule` repository and enter the `sciencecapsule` directory:

```sh
git clone git@bitbucket.org:sciencecapsule/sciencecapsule
cd sciencecapsule
```

#### Set up environment and install packages

From the root directory of the repository, install the dependencies in a Conda environment called `sciencecapsule`:

```sh
conda env create --file dependencies/bare-metal.yml
```

**NOTE** If the Conda environment already exists, re-run this command adding the `--force` option, or manually remove the environment with `conda env remove --name sciencecapsule` beforehand.

Activate the `sciencecapsule` environment.
Once the environment is active, its name will appear on the shell prompt.
All following steps assume that the Conda environment has been activated.

```sh
conda activate sciencecapsule
(sciencecapsule)
```

Install the `sc` Python package:

```sh
python -m pip install --editable .
```

Install the Node.js dependencies using npm:

```sh
cd ui/backend
npm install
cd -
```

#### Set up config directory

The `sc bootstrap` command automatically creates all necessary configuration.

Its main argument is `config_dir`, the path to the configuration directory that will be created and populated during the initialization process.
To avoid overwriting existing configurations, the directory must not already exist.
To re-run the initialization on an existing directory, delete it manually before running `sc bootstrap`.

The following `sc bootstrap` usage examples assume that the configuration directory to initialize is `$HOME/.scicap`.

With the single positional argument, the initialization will not pre-populate monitored directories.
The monitored directories can be added after the initialization is complete by editing the `events.yml` file in the newly created config directory.

```sh
sc bootstrap "$HOME/.scicap"
```

By specifying the `-d/--monitored-dir` option one or more times, they will be added to the `events.yml` file during the initialization process:

```sh
# monitored directories should exist before the monitoring services are started
mkdir -p $HOME/workdir /tmp/test
sc bootstrap "$HOME/.scicap" --monitored-dir "$HOME/workdir" --monitored-dir /tmp/test
```

The `-s/--event-sources` option is used to set the enabled event sources in `events.yml`.
If not given, the default value `auto` will result in the best supported sources being chosen depending on the target platform where the script is running.
Currently, in practice, this means enabling `inotify` on Linux, and `watchdog` on macOS and Windows:

```sh
# this is equivalente to the previous command
sc bootstrap "$HOME/.scicap" --monitored-dir "$HOME/workdir" --monitored-dir /tmp/test --event-sources auto
```

One or more values can be specified as a comma-separated list.
In this case, the chosen sources will be added to the configuration files regardless if they are available on the target platform or not.

```sh
# running this on macOS or Windows will produce valid configuration files, but the corresponding services will crash upon startup
sc bootstrap "$HOME/.scicap" --monitored-dir "$HOME/workdir" --monitored-dir /tmp/test --event-sources inotify,strace
```

#### Verify the installation

If the initialization setup finishes without errors, a message will show the steps needed to use Science Capsule using the newly created configuration directory,
as well as the ports assigned to the various services.
As long as `SC_CONFIG_DIR` is set, the `sc` commands can be issued from any directory.

```sh
export SC_CONFIG_DIR="$HOME/.scicap"

sc services start all
```

If one or more services encounter errors during startup,
a warning with the location of the log files to be checked for more detailed information will be shown.

If the services are started normally, we can test that events are captured, analyzed, and displayed correctly.

First, generate some test filesystem events:

```sh
# one of the monitored directories
cd $HOME/workdir
echo "very important results" >> doc.txt
```

Then, display the captured events, either by invoking the `sc inspect events` command; or through the WebUI, after refreshing the page.
In both cases, it might be necessary to wait a few seconds for the raw events to be saved in the database.

In addition, a real-time view of the event capture service logs can be accessed to display the events as they are being produced:

```sh
sc services tail -f capture
```

##### Verifying the installation using the test suite

In addition to the steps described above, the installation can be verified by running the Science Capsule test suite.
To run the test suite, refer to the "Running the test suite" section of this document.

### Usage

- When opening e.g. a new terminal, make sure that:
    - The `sciencecapsule` Conda env is activated,
    - The `SC_CONFIG_DIR` environment variable is set to a value that matches the one used for the initialization

#### Change logging level

Set the `SC_LOGGING_LEVEL` environment variable to any of `CRITICAL`, `WARNING`, `INFO`, `DEBUG` (the default).

#### `sc services` commands

At the moment `sc services` is a very thin wrapper around `supervisorctl`, so the supported actions/arguments are the same:

```sh
sc services {start/stop/restart} <service-name>
sc services {start/stop/restart} all

# stop all services as well as the `supervisord` process
sc services shutdown

sc services status

# clear logs for one or more services
sc services clear <service-name>
sc services clear all

# show a list of possible actions supported by the underlying supervisorctl command
sc services help

# without any argument, an interactive session will be started, supporting the same actions
sc services
```

Vanilla Supervisor resources such as e.g. `supervisorctl --help` can be used for more information.

Running `sc services` with the `--inspect-processes` flag will show all processes created by Science Capsule.
This does not rely on Supervisor, and thus can be used to e.g. verify that no orphan processes are left behind at the end of a session.

```sh
sc services --inspect-processes
```

##### Dependencies between services

If using `sc services` commands to manage individual services, keep in mind that the `mongo` services must be running for other services to work properly.

In other words, assuming that these commands are run from a fresh start, e.g. after `sc services shutdown`, the following will not work:

```sh
# The `capture` service requires the `mongo` service to be running
sc services start capture
```

But any of these will:

```sh
sc services start all
sc services start mongo capture
```

#### Accessing the WebUI

The Science Capsule WebUI can be accessed at any time while the `webui_server` service is running.
The `sc services status` command can be used to display the status of all services.

The WebUI URL is `http://localhost:<PORT>`, where `<PORT>` is the port number set dynamically when running `sc bootstrap`, and might vary depending on available ports.
The base values are 54001 on Linux and macOS, and 54002 on Windows.
The exact value can be verified in the logs for the `webui_server` services accessible at `$SC_CONFIG_DIR/services/logs/webui_server.log`.

To access the WebUI, open its URL in a web browser (currently tested on Firefox and Chrome).

#### Enabling capture of process events with strace on bare-metal (Linux only)

**IMPORTANT**  Enabling `strace` might require additional steps. Refer to [this document](./docs/enabling-strace.md) for a detailed description.

On compatible platforms, Science Capsule can capture OS process events through the Strace command-line tool available on Linux-based distributions.

To enable process event capture, open the configuration file located at `$SC_CONFIG_DIR/events.yml` and set or verify the following settings:

- `monitored_pids` is a list containing the PIDs of the process to be monitored
    - The monitoring is recursive, i.e. all descendants of each monitored process will be monitored as well
- `sources` contains the `strace` event capture source

After editing the file, restart the capture service if it was already running:

```sh
sc service restart capture
```

---

## Using Science Capsule with Docker

### Installation

- These steps were successfully tested on:
    - Linux (Ubuntu 18.04.4 and 20.04)
    - macOS (10.12.6)

#### System packages

- Git
- Docker and Docker Compose
    - Docker Compose should be included with Docker in most cases

#### Science Capsule repository

Clone the `sciencecapsule` repository and enter the `sciencecapsule` directory

```sh
git clone git@bitbucket.org:sciencecapsule/sciencecapsule
cd sciencecapsule
```

### Container workflow

#### Building a local Science Capsule Docker image

From the `docker` subdirectory of the repository,
run `docker-compose build` to build a base Science Capsule image locally:

```sh
cd docker/
docker-compose build capsule
```

Once the build step completes without errors, verify that the image has been created by running:

```sh
docker image ls
```

#### Creating and launching a Science Capsule container

The `run` subcommand command creates a running container from an existing image.
The `--name` can optionally be used to specify a human-readable name to assign to the container;
in this exampe, we'll use `my-capsule`.

From the same directory, run:

```sh
docker-compose run --name my-capsule capsule
```

The equivalent of the previous command using `docker` instead of `docker-compose` is:

```sh
docker run -it --name my-capsule sciencecapsule/base bash
```

To be able to access the WebUI from a browser running on the Docker host,
add the `-p/--publish` flag to these commands, specifying any free port on the host, e.g. `12345`,
and the WebUI port inside the container (by default, `54001`):

```sh
docker-compose run --name my-capsule --publish 12345:54001 capsule
docker run -it --name my-capsule --publish 12345:54001 sciencecapsule/base bash
```

When the container is started, all Science Capsule services are started automatically.
Once the container shell prompt appears, we are inside the container.

#### Testing that the event capture and analysis works

Inside the container, filesystem events are automatically captured by Inotify.

Try to run any command that reads, writes, modifies, deletes, or moves files or directories in the monitored `/home/capsule` directory or any of its subdirectories:

```sh
echo "A very important result" > doc.txt
cat doc.txt
echo "An update to the result" >> doc.txt
mkdir -p foo && mv doc.txt foo/
cp foo/doc.txt bar.txt
rm -r foo/doc.txt
```

Then, verify that events are captured and analyzed:

```sh
sc services tail -f capture
```

#### Accessing the WebUI

As long as the Science Capsule container is running, the WebUI can be accessed with a web browser on `localhost`
using the host-side port specified when creating the container, as described above.
in this example, the complete URL would be `http://localhost:12345`.

#### Enabling capture of process events using strace

**IMPORTANT**  Enabling `strace` within a Science Capsule container might require additional steps on the Docker host system. Refer to [this document](./docs/enabling-strace.md) for a detailed description.

Add the `--cap-add=SYS_PTRACE` and `--init` flags when starting the container with `docker run`:

```sh
docker run -it --name my-capsule --cap-add=SYS_PTRACE --init --publish 12345:54001 sciencecapsule/base bash
```

The PID of the starting process (in this example, `bash`) should be added automatically to the list of monitored PIDs before starting the Science Capsule services.

Verify that the process events are captured correctly by invoking an executable program (i.e. not a shell built-in), such as `ls`, and then inspect the event captured using one of the methods described above.

#### Stopping and restarting the container

By pressing CTRL-D, the container will be exited.
All running subprocesses, including the Science Capsule services, will be stopped as well.

To restart and re-enter the container, run:

```sh
docker start -ia my-capsule
```

The Science Capsule services will be restarted automatically.
Everything inside the container, including the events previously captured and analyzed by Science Capsule,
are in the same state as when the container was stopped.

#### Saving and exporting the container

The state of a container can be saved, exported, and transferred as a single tar archive.
The tar archive can then be loaded on any other Docker host.

Run `docker commit` to an image with the complete state of a stopped container:

```sh
docker commit my-capsule sciencecapsule/my-capsule:v1
```

Then, run `docker save` to export the image to a tar archive.
Use the same image label as in the previous step; the filename is not relevant and can be anything:

```sh
docker save sciencecapsule/my-capsule:v1 > my-capsule-v1.tar
```

The file, although relatively large, can be copied or transferred as normal,
e.g. using a removable drive, a cloud storage provider, SFTP/rsync, etc

#### Loading the exported image

On any Docker host, run the `docker load` command to convert back the tar archive into a Docker image:

```sh
docker load < my-capsule-v1.tar
```

Once the image is loaded, verify that it appears in `docker image ls`.
Then, run `docker run` to create and launch a container from the image.
The container ID will be different, but the state will be identical to the original container:

```sh
docker run -it --name my-capsule --publish 12345:54001 sciencecapsule/my-capsule:v1
```

At this point, the workflow will be identical to the steps described in the previous section,
including all applicable command-line options given to the `docker run` command.

## Running the test suite

From the root of the cloned Science Capsule repository,
after activating the environment where Science Capsule is installed,
run the following command to install the package with the additional dependencies needed for running the unit tests:

```sh
# the quotes might be needed to avoid the square brackets to be interpreted by the shell
python -m pip install -e '.[tests]'
```

Then, invoke the `pytest` command to run the test suite:

```sh
pytest tests/
```

## Troubleshooting and known issues

If you run into an issue, refer to the [Science Capsule issue tracker](https://bitbucket.org/sciencecapsule/sciencecapsule/issues) to check if the issue matches any of the known issues already there.
If this doesn't seem to be the case, please feel free to [create a new issue](https://bitbucket.org/sciencecapsule/sciencecapsule/issues/new) describing the problem.
