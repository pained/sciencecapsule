# Enabling capture of process events with Strace

On compatible platforms, Science Capsule can capture OS process events through the Strace command-line tool available on Linux-based distributions.

At the moment, the following platforms are supported:

- Linux bare metal
- Docker on Linux host
- Docker on macOS host

Additional preliminary setup is required for all of these platforms.
See below for context and steps.

## Preliminary setup

The process event capture capabilities in Science Capsule rely on Strace being able to "attach" itself to running processes.
In most recent Linux distributions, this capabilty depends on a kernel runtime setting called `kernel.yama.ptrace_scope`.

For the Science Capsule process event capture to work, a value of 0 is required,
however this is different from the default value (generally 1).

The current value can be verified on Linux without superuser privileges by running:

```sh
sysctl kernel.yama.ptrace_scope
```

If the value is not 0, it must be set to 0 before being able to capture process events.
The setup steps vary depending on the platform, and are described in the following sections.

Since this is a kernel-level setting, all of these steps require either:

- Running a command on the host system with root/superuser privileges
- Running a command in a container with the `--privileged` flag

### Linux bare metal

If the `strace` binary is not already installed on the system, install it using the default package manager.

The `kernel.yama.ptrace_scope` setting can be set for the current session (i.e. until the system is rebooted) using the `sysctl` tool as `root` or with `sudo`:

```sh
sysctl -w kernel.yama.ptrace_scope=0
```

To persist this setting across reboots, instead set the value in the `/etc/sysctl.d/10-ptrace.conf` file (also requires superuser privileges):

```sh
# /etc/sysctl.d/10-ptrace.conf
kernel.yama.ptrace_scope = 0
```

The setting should be automatically applied at the next system reboot.

### Docker on Linux host

If the above steps are performed before running a Science Capsule container,
the process event capture will be available within the container.

To automatically configure and enable process event capture when the container is started,
run the container with the `--cap-add SYS_PTRACE` flag:

```sh
docker run -it --init --name my-capsule --cap-add SYS_PTRACE sciencecapsule/base bash
```

In this case, the PID of the command passed to `docker run` (in this case `bash`) will be automatically configured in the list of monitored PIDs.
Events originating from this process (including its child processes recursively) will be captured automatically.

Without the `--cap-add SYS_PTRACE` flag, the prerequisites for the process event capture will still be fulfilled,
but the `strace` event source will not be added to the list of enabled event sources.

If needed, the configuration file with these settings can be accessed inside the container at `$SC_CONFIG_DIR/events.yml`.

### Docker on macOS host

Unlike on a Linux host, on Docker for macOS there seems to be no officially documented way to apply kernel settings directly from the host system.

One strategy for doing this is running the command in a one-off, non-interactive Docker container.

The Science Capsule Docker image will be used for these examples, but most base images should be able to run the required commands.

To check the current value of `kernel.yama.ptrace_scope`, run:

```sh
docker run --rm --env SC_NO_AUTOSTART=1 sciencecapsule/base sysctl kernel.yama.ptrace_scope
```

If the value is not 0, it can be set using a privileged container with user set to `root`:

```sh
docker run --rm --env SC_NO_AUTOSTART=1 --privileged --user root sciencecapsule/base sysctl -w kernel.yama.ptrace_scope=0
```

After running this command once, all subsequent Science Capsule containers run during the current session will be capable of capturing process events without requiring the `--privileged` flag.
As described above, adding the `--cap-add SYS_PTRACE` flag will automatically configure and activate process event capture when the container is started:

```sh
docker run -it --init --name my-capsule --cap-add SYS_PTRACE sciencecapsule/base bash
```

Similarly to what described for the Linux case, this setting will not be persisted when the system is rebooted,
and should be run whenever needed after a reboot (or, depending on the Docker startup configuration, possibly within a login shell script).

## General recommendations for process event capture

When running a Science Capsule container with process event capture enabled,
it is strongly recommended to use the `--init` flag of `docker run`:

```sh
docker run -it --init --name my-capsule --cap-add SYS_PTRACE sciencecapsule/base bash
```

Without the `--init` flag, the command passed to `docker run` (in this case `bash`) will have PID=1,
which has been shown to cause issues with Strace.
