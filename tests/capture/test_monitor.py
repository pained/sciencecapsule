from pathlib import Path
import platform
import shutil
import tempfile
import time

import pytest
import sys

import actions as _actions


def make_manager(monitored_dirs, sources):

    from sc.capture.core import Settings, CaptureManager

    manager = CaptureManager()
    settings = Settings(
        {
            'monitored_dirs': monitored_dirs,
            'recursive': True
        }
    )

    manager.init_sources(sources, settings)

    return manager


def make_actions():

    class Actions(_actions.ActionHandler):
        base_dir = Path(tempfile.mkdtemp())
        create_file = _actions.CreateFile(path=base_dir / 'foo.txt', exist_ok=False)
        append_to_file = _actions.WriteTextToFile(path=create_file.path, n_lines=10_000, mode='a+')
        append_again = _actions.WriteTextToFile(path=create_file.path, n_lines=10_000, mode='a+')
        read_content = _actions.ReadFromFile(path=append_again.path, mode='tr')
        # move_file = _actions.MoveFile(src_path=create_file.path, dst_path=base_dir / 'bar.txt')
        # copy_file = _actions.CopyFile(src_path=move_file.dst_path, dst_path=base_dir / 'bar-copy.txt')
        # delete_file = _actions.DeleteFile(path=move_file.dst_path)
        # add_content_to_copy = _actions.WriteTextToFile(wait_before=2, path=copy_file.dst_path, n_lines=1, mode='ta')

        def cleanup(self):
            shutil.rmtree(self.base_dir)

    obj = Actions()
    return obj


def _get_uncommitted_raw_events(monitor):
    return list(monitor.events._deque)


def _get_raw_events(monitor):
    return list(monitor.events.all())


class TestCaptureSingleSource:


    @pytest.fixture(autouse=True, scope='class',
        params=[
            'watchdog',
            pytest.param('inotify',
                marks=pytest.mark.skipif(
                    platform.system() != 'Linux',
                    reason="inotify is only available on Linux"
                )
            )
        ]
    )
    def setup(self, request, mock_db_uri):
        from sc.db import models
        from sc.capture.sources import watchdog, inotify

        source = request.param
        sources = [source]

        actions = make_actions()
        # assert source != 'inotify'
        manager = make_manager(monitored_dirs=[actions.base_dir], sources=sources)

        request.cls.actions = actions
        request.cls.manager = manager
        request.cls.source = source
        request.cls.sources = sources

        models.connect(mock_db_uri)

        manager.start_monitoring()
        request.cls.action_events_map = self.get_events_for_actions()

        yield

        # TODO clean up (e.g. actions.base_dir)
        manager.stop_monitoring()
        actions.cleanup()

    @pytest.mark.skipif(sys.platform != "darwin", reason="does not run on linux")
    def test_manager_is_initialized_correctly(self):
        assert len(self.manager) == len(self.sources), 'All requested sources should be loaded'
        assert self.active_adaptor.source == self.source

    # def test_manager_starts_capturing_without_errors(self):
    #     self.manager.start_monitoring()

    def wait(self, timeout_s):
        time.sleep(timeout_s)

    def wait_before_capture(self):
        self.wait(1)

    @property
    def active_adaptor(self):
        return self.manager.adaptors[0]

    def get_events_for_action(self, action):
        # monitor = self.active_adaptor.monitor
        print(f'action={action}')
        action()
        self.wait_before_capture()
        events = self.manager.capture()

        return events

    def get_events_for_actions(self):
        action_events_map = {}
        for action in self.actions:
            action_events_map[action] = self.get_events_for_action(action)

        return action_events_map

    def is_path_equal(self, event, action_path):
        return event.artifact.path == str(action_path)

    @pytest.mark.skipif(sys.platform != "darwin", reason="does not run on linux")
    def test_file_creation(self):
        action = self.actions.create_file
        events = self.action_events_map[action]

        assert len(events) == 1
        assert self.is_path_equal(events[0], action.path)

    @pytest.mark.skipif(sys.platform != "darwin", reason="does not run on linux")
    def test_file_write_without_creation(self):
        action = self.actions.append_again
        events = self.action_events_map[action]

        explanation = (
            'Writing to an already existing file should be captured as a single event, '
            'however in a fraction of cases it happened that multiple events are generated instead. '
            'This does not seem to happen consistently, '
            "and so it's possible that the test will succeed when run again."
        )
        assert len(events) == 1, explanation
        assert self.is_path_equal(events[0], action.path)

    @pytest.mark.skipif(sys.platform != "darwin", reason="does not run on linux")
    def test_file_read(self):
        action = self.actions.read_content
        events = self.action_events_map[action]

        if self.source == 'inotify':
            assert events
            assert self.is_path_equal(events[0], action.path)
        elif self.source == 'watchdog':
            assert not events
