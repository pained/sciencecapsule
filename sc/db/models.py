import enum
import collections
import datetime as dt
import logging
import time
import types

import pymongo

from pymodm import MongoModel, fields, manager, errors
from pymodm import connect as _connect

from sc.db.mongo import _get_mongo_uri


LOG = logging.getLogger(__name__)


_DB_CONNECTION_RETRY_TIMEOUTS = [0.1, 1, 3, 5, 10]


def connect(mongo_uri=None, retry_timeouts=_DB_CONNECTION_RETRY_TIMEOUTS):
    mongo_uri = mongo_uri or _get_mongo_uri()

    LOG.info('Attempting to connect to DB using URI: %s', mongo_uri)
    n_retries = len(retry_timeouts)
    for attempt_number, timeout_s in enumerate(retry_timeouts, 1):
        LOG.info('Attempt %d/%d, timeout: %.1f s', attempt_number, n_retries, timeout_s)
        timeout_ms = timeout_s * 1000
        timeout_kwargs = {
            'connectTimeoutMS': timeout_ms,
            'serverSelectionTimeoutMS': timeout_ms,
        }
        try:
            _connect(mongo_uri, **timeout_kwargs)
            _ConnectionLog.objects.create()
        except pymongo.errors.ConnectionFailure as e:
            db_error = e
            LOG.warning('Attempt to connect to DB failed: %s', repr(db_error))
        except ValueError as invalid_url:
            LOG.error('The URI provided seems to be invalid: %s', repr(invalid_url))
            db_error = invalid_url
        else:
            LOG.info('Connection successful.')
            return
    else:
        raise errors.ConnectionError(f'Could not connect to DB after {n_retries} attempts') from db_error


def get_field_names(model):
    return [field.attname for field in model._mongometa.fields_ordered]


def get_model_name(model):
    return model._mongometa.object_name


class UtilQuerySet(manager.QuerySet):

    def to_json(self):
        import json

        return json.dumps(list(self.values()), default=str, indent=4)

    def to_dataframe(self, keep_internal_fields=True, normalize=False, nested_field_sep='.'):
        import pandas as pd
        from pandas.io.json import json_normalize

        internal_fields = ['_cls', '_id']
        data_fields = get_field_names(self._model)

        if keep_internal_fields:
            columns = data_fields
        else:
            columns = [name for name in data_fields if name not in internal_fields]

        data = list(self.values())

        if normalize:
            df = json_normalize(data, sep=nested_field_sep)
        else:
            df = pd.DataFrame(data, columns=columns)

        return df

    def get_last_inserted(self, field='_id'):
        try:
            obj = (self
                   .order_by([(field, pymongo.DESCENDING)])
                   .first()
                  )
        except Exception as e:
            obj = None

        return obj

    def order_by_field(self, field_specs):
        """
        Convenience method to allow sorting using a Django-like syntax
        using a list of field names with a leading or trailing '-' to signifiy descending order.
        """

        order_by_tuples = []

        for spec in field_specs:
            if spec.startswith('-'):
                field = spec[1:]
                order = pymongo.DESCENDING
            elif spec.endswith('-'):
                field = spec[:-1]
                order = pymongo.DESCENDING
            else:
                field = spec
                order = pymongo.ASCENDING
            order_by_tuples.append((field, order))

        return self.order_by(order_by_tuples)


class UtilManager(manager.Manager):

    def __init__(self, *args, **kwargs):
        self._logger = None
        super().__init__(*args, **kwargs)

    @property
    def log(self):
        if not self._logger:
            components = (
                self.__class__.__module__,
                self.__class__.__name__,
                self.model.__name__,
            )
            logger_name = str.join('.', components)
            self._logger = logging.getLogger(logger_name)
        return self._logger

    def get_queryset(self):
        return UtilQuerySet(model=self.model)


class Deque(collections.deque):

    def dropn(self, n):
        # in case n > len(self)
        n_droppable = min(n, len(self))
        for _ in range(n_droppable):
            self.pop()


class StagingManager(UtilManager):
    "Creates or adds model instances in memory before performing a bulk write (triggered manually)"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._deque = Deque()

    @property
    def n_staged(self):
        return len(self._deque)

    def create(self, **kwargs):
        instance = self.model(**kwargs)
        self.add(instance)

        return instance

    def add(self, *objs):
        self._deque.extendleft(objs)

    def commit(self):
        n_committed = None

        # we take the reverse to reproduce the original insertion order
        # since deque.extendleft() is used to add items
        to_commit = list(reversed(self._deque))
        n_to_commit = len(to_commit)

        if to_commit:

            self.log.debug('number of staged items: %d', self.n_staged)
            try:
                inserted_ids = self.bulk_create(to_commit)
            except Exception as e:
                self.log.exception('Insertion of %d items was not successful', n_to_commit)
            else:
                n_committed = len(inserted_ids)
                self.log.info('Saved %d events to the DB', n_committed)
                self._deque.dropn(n_to_commit)
            finally:
                self.log.debug('number of staged items: %d', self.n_staged)

        return n_committed


class _ConnectionLog(MongoModel):
    """
    Used internally to test whether the connection to the DB is successful
    """
    timestamp = fields.DateTimeField(default=dt.datetime.utcnow)

    objects = UtilManager()


class EventProcessingLogQuerySet(UtilQuerySet):

    def latest(self):
        try:
            return (
                self
                # TODO do we want to have a different ordering field on the model specifically?
                .order_by([('_id', pymongo.DESCENDING)])
                .first()
            )
        except errors.DoesNotExist:
            return None

    def for_model(self, model):

        return (
            self
            .raw({'model_name': get_model_name(model)})
        )


class EventProcessingLog(MongoModel):
    # TODO this clearly needs a timestamp
    model_name = fields.CharField()
    processed_ids = fields.ListField(fields.ObjectIdField(), required=False)
    data_last_processed = fields.DictField(required=False)
    extra_info = fields.DictField(required=False)

    objects = manager.Manager.from_queryset(EventProcessingLogQuerySet, 'EventProcessingLogManager')()


# TODO maybe this could be called "intervalqueryset?"
class IncrementalQuerySet(UtilQuerySet):
    """
    QuerySet with fixed range for incremental processing of a ranged batch of items.
    """

    @property
    def range_field(self):
        # TODO should we assume/assert that self._query is always a dict with one single key?
        return list(self._query.keys())[0]

    @property
    def range(self):
        # TODO does this make sense???
        query_dict = self._query[self.range_field]
        low = query_dict.get('$gt')
        high = query_dict.get('$lte')
        return low, high

    def ordered(self, direction=pymongo.ASCENDING):
        return self.order_by(
            [
                (self.range_field, direction)
            ]
        )

    def first(self):
        return self.ordered(pymongo.ASCENDING).first()

    def last(self):
        return self.ordered(pymongo.DESCENDING).first()


class IncrementalManager(UtilManager):

    def __init__(self, *args, range_field='_id', **kwargs):
        super().__init__(*args, **kwargs)
        # TODO maybe range_field should also accept pymodm.fields.BaseMongoField instances
        # and extract the name, to have some additional typiness
        self._range_field = range_field

    @property
    def model_name(self):
        return get_model_name(self.model)

    def processing_logs(self):
        return EventProcessingLog.objects.for_model(self.model)

    def get_range_query(self, field):
        log_entry = self.processing_logs().latest()

        if log_entry is None:
            low = None
        else:
            prev_low, prev_high = log_entry.data_last_processed.get('range', (None, None))
            low = prev_high

        # TODO this could also actually not exist if objects.all() is empty
        # idea: this could raise NoRawEventsToProcess at this point
        # and, actually, we don't even have to worry too much about special cases
        # because we can just catch self.model.DoesNotExist in all cases (incremental or otherwise)

        # basically, to get the upper limit, we already have to perform the query...
        high = (
            self.model
            .objects.all()
            .order_by([(field, pymongo.DESCENDING)])
            .values()
            .first()
        )[field]

        query = {field: {'$lte': high}}

        if low:
            query[field].update({'$gt': low})

        return query

    def since_last_processed(self):
        query = self.get_range_query(self._range_field)
        return IncrementalQuerySet(model=self.model, query=query)

    def register_processed(self, queryset, **kwargs):

        log_entry = self.processing_logs().create(
            model_name=self.model_name,
            data_last_processed={
                'range': queryset.range,
                # we can't store the raw query because of DictField validation
                # 'raw_query': queryset.raw_query
            },
            extra_info=kwargs,
            # TODO also add timestamp?
            # TODO also store the queryset ids?
        )

        # log_entry.save()
        # also cache log_entry in memory?

        # TODO and maybe here we could return the query to make for the next one?...


class RawEvent(MongoModel):
    "Raw events are produced by various monitoring sources that need to be stored and analyzed to produce Events"
    timestamp_ns = fields.BigIntegerField(
        verbose_name="Timestamp (ns)",
        required=True
    )
    # override with the appropriate event type
    event_data = ...

    objects = UtilManager()
    staged = StagingManager()
    incremental = IncrementalManager()


class EnumField(fields.MongoBaseField):
    def __init__(self, enum_class, *args, **kwargs):
        kwargs['choices'] = list(enum_class)
        super().__init__(*args, **kwargs)
        self._enum_class = enum_class

    def to_mongo(self, enum_obj):
        return enum_obj.name

    def to_python(self, value):
        try:
            enum_obj = self._enum_class(value)
        except (KeyError, TypeError) as e:
            raise TypeError(f'Invalid Enum member or key "{value}"') from e

        return enum_obj


class _ObjectProxy(types.SimpleNamespace):
    """
    Utility class to allow object-style attribute access for model instance attributes in Python code
    while keeping the full genericity of a generic pymodm.fields.DictField at the database level.
    """

    def keys(self):
        public_fields = (f for f in self.__dict__ if not f.startswith('_'))
        return public_fields

    def __iter__(self):
        return iter(self.keys())

    def __contains__(self, query):
        return query in self.keys()

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, name, value):
        try:
            setattr(self, name, value)
        except Exception as e:
            raise ValueError('"name" must be a valid Python identifier') from e


class ObjectProxyField(fields.DictField):
    """
    A field that wraps a regular Python dict to enable object-style attribute access.
    """

    def __init__(self, *args, factory=_ObjectProxy, dict_factory=dict, **kwargs):
        kwargs.setdefault('default', factory)
        super().__init__(*args, **kwargs)
        self._obj_factory = factory
        self._dict_factory = dict_factory

    def to_python(self, raw_value):
        d = super().to_python(raw_value)
        res = self._obj_factory(**d)
        return res

    def to_mongo(self, python_value):
        d = self._dict_factory(python_value)
        res = super().to_mongo(d)
        return res


# inherit from str for easier serialization/deserialization
class EventPropertyEnum(str, enum.Enum):
    def __repr__(self):
        type_name = type(self).__qualname__
        return f'<{type_name}.{self.name}>'

    def __str__(self):
        return str(self.name)


class _NamespaceProxy:
    """
    Utility class to define and access together different objects that are conceptually related.
    """


class filesystem(_NamespaceProxy):
    class Action(EventPropertyEnum):
        create = 'create'
        delete = 'delete'
        write = 'write'
        modify = 'modify'
        modify_content = 'modify_content'
        modify_attrib = 'modify_attrib'
        move = 'move'
        read = 'read'

    class ArtifactType(EventPropertyEnum):
        file = 'file'
        directory = 'directory'


class process(_NamespaceProxy):
    class Action(EventPropertyEnum):
        start = 'start'
        exit = 'exit'
        create = 'create'

    class ArtifactType(EventPropertyEnum):
        process = 'process'


class Event(MongoModel):

    source = fields.CharField()
    time = fields.DateTimeField(required=False)
    duration = fields.FloatField(required=False, default=0)

    artifact_type = EnumField(EventPropertyEnum, required=False)
    action = EnumField(EventPropertyEnum, required=False)

    artifact = ObjectProxyField(required=False)

    objects = UtilManager()


class FilesystemEvent(Event):
    action = EnumField(filesystem.Action, required=False)
    artifact_type = EnumField(filesystem.ArtifactType, required=False)

    objects = UtilManager()


filesystem.Event = FilesystemEvent


class ProcessEvent(Event):
    action = EnumField(process.Action, required=False)
    artifact_type = EnumField(process.ArtifactType, default=process.ArtifactType.process)

    objects = UtilManager()


process.Event = ProcessEvent


class CurrentUIEvent(MongoModel):
    """
    Model representing the schema used by the current version of the UI.
    """

#     oid = fields.CharField()
    artifact = fields.CharField()
#     owner = fields.CharField()
    artifact_uri = fields.CharField()
    artifact_type = fields.CharField()

    event_src = fields.CharField(default="NA")
    event_type = fields.CharField()
    event_time = fields.DateTimeField()

    snapshot_id = fields.IntegerField(default=0)
    pid = fields.IntegerField(required=False)

    @classmethod
    def from_event(cls, event_obj):
        """
        Creates an instance of this model by adapting the data from a different event class.
        """
        self = cls()

        self.event_type = str(event_obj.action).upper()
        self.event_time = event_obj.time

        if isinstance(event_obj, FilesystemEvent):
            artifact = str(event_obj.artifact.path)
            artifact_type = "DATA"

        if isinstance(event_obj, ProcessEvent):
            artifact = str.join(' ', event_obj.artifact.argv)
            # artifact = event_obj.artifact.exepath
            artifact_type = "EXE"

        self.artifact = artifact
        self.artifact_type = artifact_type

        return self

    objects = UtilManager()
