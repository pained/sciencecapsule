import logging
from pathlib import Path
import os


LOG = logging.getLogger(__name__)


def _get_config_dir():
    default = Path.home() / '.scconfig'

    LOG.debug('Looking if $CONFIG_DIR is set as an environment variable...')
    from_env = os.environ.get('SC_CONFIG_DIR', None)

    if from_env is not None:
        LOG.debug(f'$SC_CONFIG_DIR={from_env}. Using this value.')
        config_dir = from_env

    else:
        LOG.debug(f'$CONFIG_DIR not set. Using default value: {default}')
        config_dir = default

    config_dir = Path(config_dir).absolute()

    LOG.info(f'CONFIG_DIR is set to {config_dir}')

    return config_dir


CONFIG_DIR = _get_config_dir()

SERVICES_DIR = CONFIG_DIR / 'services'
SERVICES_LOG_DIR = SERVICES_DIR / 'logs'
